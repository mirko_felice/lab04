package u04lab.code

import Optionals._
import Lists._
import Streams.Stream

import scala.util.Random

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

object PowerIterator {
  def apply[A](stream: Streams.Stream[A]): PowerIterator[A] = PowerIteratorImpl(stream)

  private case class PowerIteratorImpl[A](private var stream: Stream[A]) extends PowerIterator[A] {

    private var pastList: List[A] = List.Nil()

    override def next(): Option[A] = Stream.take(stream)(1) match {
        case Stream.Empty() => Option.empty
        case Stream.Cons(head, _) =>
          lazy val h = head()
          stream = Stream.drop(stream)(1)
          pastList = List.append(pastList, List.Cons(h, List.Nil()))
          Option.of(h)
    }

    override def allSoFar(): List[A] = pastList

    override def reversed(): PowerIterator[A] = PowerIterator(Stream.fromList(List.reverse(pastList)))
  }
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]): PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  def fromStream[A](stream: Stream[A]): PowerIterator[A] = PowerIterator(stream)

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = PowerIterator(Stream.iterate(start)(successive))

  override def fromList[A](list: List[A]): PowerIterator[A] = PowerIterator(Stream.fromList(list))

  override def randomBooleans(size: Int): PowerIterator[Boolean] = PowerIterator(Stream.take(Stream.generate(Random.nextBoolean()))(size))
}
