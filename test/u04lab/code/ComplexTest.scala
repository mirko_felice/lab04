package u04lab.code

import org.junit.jupiter.api.Assertions.{assertEquals, assertFalse, assertNotEquals, assertTrue}
import org.junit.jupiter.api.Test

class ComplexTest {

  @Test def testComplexClass() {
    val c1 = Complex(20, 30)
    val c2 = Complex(20, 30)
    assertFalse(c1 equals c2)
    assertFalse(c1 == c2)
    assertFalse(c1 eq c2)

    assertNotEquals("ComplexImpl(20.0,30.0)", c1.toString)

    val sum = c1 + c2
    assertEquals(Complex(40, 60).re, sum.re)
    assertEquals(Complex(40, 60).im, sum.im)

    val prod = c1 * c2
    assertEquals(Complex(-500, -1200).re, prod.re)
    assertEquals(Complex(-500, -1200).im, prod.im)
  }

  @Test def testComplexCaseClass() {
    val c1 = Complex.applyCase(20, 30)
    val c2 = Complex.applyCase(20, 30)
    assertTrue(c1 equals c2)
    assertTrue(c1 == c2)

    assertEquals("ComplexCaseImpl(20.0,30.0)", c1.toString)

    val sum = c1 + c2
    assertEquals(Complex.applyCase(40, 60), sum)

    val prod = c1 * c2
    assertEquals(Complex.applyCase(-500, -1200), prod)

  }
}
