package u04lab.code

import org.junit.jupiter.api.Assertions.{assertEquals, assertTrue}
import org.junit.jupiter.api.Test
import Lists._
import u04lab.code.Lists.List._

class MoreTest {

  @Test def testListFactory() {
    val l1 = List(1, 2, 3)
    assertEquals(List.Cons(1, List.Cons(2, List.Cons(3, List.Nil()))), l1)
    val l2 = List()
    assertEquals(List.Nil(), l2)
  }

  @Test def testExtractor() {
    val c1 = Course("PPS", "Viroli")
    val c2 = Course("PCD", "Viroli")
    val c3 = Course("OOP", "Viroli")
    val courses1 = List(c1, c2, c3)
    courses1 match {
      case sameTeacher(t) => assertTrue(t.nonEmpty); println(s"$courses1 have same teacher $t")
      case _ => println(s"$courses1 have different teachers")
    }
    val c4 = Course("PCD", "Ricci")
    val courses2 = List(c1, c4, c3)
    courses2 match {
      case sameTeacher(t) => assertTrue(t.nonEmpty); println(s"$courses2 have same teacher $t")
      case _ => println(s"$courses2 have different teachers")
    }
  }

  object sameTeacher {
    def unapply(list: List[Course]): Option[String] = list match {
      case Cons(head, tail) if length(filter(tail)(_.teacher == head.teacher)) == length(tail) => Option(head.teacher)
      case _ => Option.empty
    }
  }
}
